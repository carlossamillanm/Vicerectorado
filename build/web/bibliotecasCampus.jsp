<!DOCTYPE HTML>
<html>
    <head>
        <title>VIR | Biblioteca</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Business_Blog Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="css/style.css" rel='stylesheet' type='text/css' />	
        <link href="css/bibliotecaCampus.css" rel='stylesheet' type='text/css' />	
        <link href="css/css-f4b41.css" rel='stylesheet' type='text/css' />
        <link href="css/font-awesome.min.css" rel='stylesheet' type='text/css' />	
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <jsp:include  page="Menu.jsp"></jsp:include>
            <div class="container">
                <div class="bottom_content">  
                    <h3 class="titulo">Bibliotecas</h3>
                    <div class="col-md-6  portfolio-left">
                        <div class="thumbnail">
                            <div class="event-img">
                                <img src="images/bibliotecaCampus/biblioUNPRG.jpg" alt="..." >
                                <div class="over-image"></div>
                            </div>
                            <div class="caption">
                                <h4><a href="#">Lorem Ipsum</a></h4>
                                <p>Mauris diam massa, malesuada a sapien in, semper vehicula erat. Vivamus sagittis leo a ullamcorper ultricies. Suspendisse placerat mattis arcu nec por</p>
                                <span>
                                    <a href="#">School Studies</a>
                                    <a href="#">College Studies</a>
                                </span>
                                <a href="#">
                                    <span><i class="fa fa-chain chain_1"></i>VIEW PROJECT</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 portfolio-left">
                        <div class="thumbnail">
                            <div class="event-img">
                                <img src="images/bibliotecaCampus/biblioUNPRG.jpg" alt="..." >
                                <div class="over-image"></div>
                            </div>
                            <div class="caption ">
                                <h4><a href="#">Lorem Ipsum</a></h4>
                                <p>Mauris diam massa, malesuada a sapien in, semper vehicula erat. Vivamus sagittis leo a ullamcorper ultricies. Suspendisse placerat mattis arcu nec por</p>
                                <span>
                                    <a href="#">School Studies</a>
                                    <a href="#">College Studies</a>
                                </span>
                                <a href="#">
                                    <span><i class="fa fa-chain chain_1"></i>VIEW PROJECT</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 portfolio-left">
                        <div class="thumbnail">
                            <div class="event-img">
                                <img src="images/bibliotecaCampus/biblioUNPRG.jpg" alt="..." >
                                <div class="over-image"></div>
                            </div>
                            <div class="caption ">
                                <h4><a href="#">Lorem Ipsum</a></h4>
                                <p>Mauris diam massa, malesuada a sapien in, semper vehicula erat. Vivamus sagittis leo a ullamcorper ultricies. Suspendisse placerat mattis arcu nec por</p>
                                <span>
                                    <a href="#">School Studies</a>
                                    <a href="#">College Studies</a>
                                </span>
                                <a href="#">
                                    <span><i class="fa fa-chain chain_1"></i>VIEW PROJECT</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 portfolio-left">
                        <div class="thumbnail">
                            <div class="event-img">
                                <img src="images/bibliotecaCampus/biblioUNPRG.jpg" alt="..." >
                                <div class="over-image"></div>
                            </div>
                            <div class="caption ">
                                <h4><a href="#">Lorem Ipsum</a></h4>
                                <p>Mauris diam massa, malesuada a sapien in, semper vehicula erat. Vivamus sagittis leo a ullamcorper ultricies. Suspendisse placerat mattis arcu nec por</p>
                                <span>
                                    <a href="#">School Studies</a>
                                    <a href="#">College Studies</a>
                                </span>
                                <a href="#">
                                    <span><i class="fa fa-chain chain_1"></i>VIEW PROJECT</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
        <jsp:include  page="Footer.jsp"></jsp:include>
        <script src="js/move-top.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $().UItoTop({easingType: 'easeOutQuart'});
            });
        </script>
        <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 0;"></span>To Top</a>

    </body>
</html>